# Installation du projet : 

Pour installer ce projet et avoir accès à l'API fonctionnelle : 

```pip 3 install Flask && pip3 install Flask-PyMongo && pip3 install pymongo```

Veuillez ensuite lancer le serveur à la racine du projet cloné avec : 

```python3 app.py```


Alors vous attérirez sur la route racine du projet, qui est aussi la route (/api/v1) ou vous pourrez apercevoir les différentes routes utilisées pour le fonctionnement de l'API REST.

Tout d'abord d'un côté, il y a une route vous permettant de consulter toutes vos veilles et aussi d'en ajouter (méthode POST)

D'autre part, vous pourrez séléctionner la veille voulue pour voir ses informations, les modifier ou la supprimer selon l'ID que vous choisirez de passer dans l'URL.

### Merci de votre lecture ! 