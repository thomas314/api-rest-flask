from flask import Flask, render_template, request, redirect, url_for, jsonify
from flask_pymongo import pymongo
from pymongo import MongoClient
from bson.objectid import ObjectId

CONNECTION_STRING = "mongodb+srv://root:root@api-0jehb.mongodb.net/test?retryWrites=true&w=majority"
client = pymongo.MongoClient(CONNECTION_STRING)
db = client.get_database('API')
user_collection = pymongo.collection.Collection(db, 'tools')
  
# creating a Flask app 
app = Flask(__name__) 
  
@app.route('/')
@app.route('/api/v1/', methods = ['GET'])
def home():
    '''
    API DOC : 
    3 endpoints :
    @app.route('/api/v1/') -> APIDOC

    @app.route('/api/v1/tools') 

        GET method : Liste toutes les veilles
        POST method : Crée une nouvelle veille
    
    @app.route('/api/v1/tools/{id}')

        GET method : Donne les informations d'une veille
        PUT method: Modifie une veille existante
        DELETE method: Supprime une veille existante
    '''
    return render_template('index.html')

@app.route('/api/v1/tools/', methods = ['GET','POST']) 
def tools():
    if request.method == 'GET':
        #Display the all Tools
        tools=db.tools.find({})
        result=[]

        for tool in tools:
            result.append({'_id': str(tool['_id']), 'titre': tool['titre'], 'description': tool['description'], 'lien': str(tool['lien']), 'auteur': tool['auteur'] })

        # print(result)
        return jsonify(result)
    else:
        titre=request.form['titre']
        description=request.form['description']
        lien=request.form['lien']
        auteur=request.form['auteur']

        db.tools.insert_one({'titre': titre, 'description': description, 'lien':lien, 'auteur':auteur})
        return redirect('/api/v1/tools/')

@app.route('/api/v1/tools/<string:key>', methods=['GET','PUT','DELETE'])
def tool(key):
    if request.method == 'GET':
        # See a tool 
        cle = {'_id': ObjectId(key)}
        tool=db.tools.find(cle)
        result = []
        for liste in tool:
            result.append({'_id': str(liste['_id']), 'titre': liste['titre'], 'description': liste['description'], 'lien': str(liste['lien']), 'auteur': liste['auteur'] })
        # print(result)
        return jsonify(result)
        

    if request.method == 'PUT':
        # Update a tool
        titre=request.form['titre']
        description=request.form['description']
        lien=request.form['lien']
        auteur=request.form['auteur']
        db.tools.update_one({'titre': titre, 'description': description, 'lien':lien, 'auteur':auteur})
        return redirect('/api/v1/tools/<key>')

    if request.method == 'DELETE':
        # Delete a tool
        cle = {'_id': ObjectId(key)}

        db.tools.delete_one(cle)
        return redirect('/api/v1/tools/',{'message':'Veille supprimée'})

# driver function
if __name__ == '__main__': 
  
    app.run(port=5000,debug = True) 